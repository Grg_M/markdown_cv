---
layout: cv
title: Giorgio Marinato CV
---
# Giorgio Marinato
<br/>

PhD Student<br/>
CIMeC - Center for Mind and Brain Sciences<br/>
University of Trento, Italy
 
 <a href="giorgio.marinato@unitn.it">giorgio.marinato [at] unitn.it</a>


<div id="webaddress">
  <a href="https://gitlab.com/grg_m"><i class="fab fa-gitlab"></i> gitlab: GrG_M</a> - 
  <a href="https://www.cimec.unitn.it/en/178/magnetoencephalography-lab-meg-lab"><i class="fas fa-users"></i> CIMeC MEG Lab (group)</a><br/>
  <a href="https://orcid.org/0000-0001-8856-5003"><i class="ai ai-orcid"></i> OrcID: 0000-0001-8856-5003</a> - 
  <a href="https://www.researchgate.net/profile/Giorgio_Marinato"><i class="ai ai-researchgate"></i> RG: Giorgio_Marinato</a> 
</div>



## Interested in

Opportunities at the intersection between auditory cognitive neuroscience and open processes for reproducible neuroscience.<br/>
Also theories of consciousness and brain network dynamics.


### Experience in

Designing of cognitive neuroscience experiments in auditory domain with Psychtoolbox, VPixx realtime hardware and EyeLink eye tracker.<br/>
Analysis of behavioral data with Matlab custom scripts<br/>
Analysis of MEG data both at sensor and source level with Brainstorm, Filedtrip and some MNE-Python.<br/>
Git versioning system, gitlab and github server.<br/>
Instructing sessions to facilitate the adoption of collaborative git workflows.



## Education

`Nov. 2016 - present`
**Ph.D., Cognitive and Brain Sciences**, *CIMeC, University of Trento*, Italy.<br/>

expected defense: early July 2020 

`Oct 2008 - Apr 2012`
**M.Sc, Clinical-Dynamic Psychology**, *University of Padova*, Padova, Italy.

`Oct 2004 - feb 2008`
**B.Sc, Psychlogy of personality and interpersonal relation**, *University of Padova*, Padova, Italy.



## Research Experience

`Dec 2015 – Nov 2016`
**Research Assistant** *CIMeC, University of Trento*, Italy<br/>
Supervison of Prof. David Melcher.<br/>
Evoked response and time frequency data analysis at sensor level of an MEG dataset involving the Continuous Flash Suppression paradigm.

`Dec 2014 - Dec 2015`
**Internship in MEG Lab** *CIMeC, University of Trento*, Italy<br/>
Supervison of Prof. Nathan Weisz.<br/>
Help running and independently running various experiments involving MEG, EEG-MEG, MEG-TACS; first approach to data analysis with fieldtrip toolbox on resting state dataset.



## Computing Skills

*Fluent:*

Matlab - Brainsotorm, FieldTrip, Psychtoolbox

Linux - Git, bash/zsh, general system administration of server side installations

*Familiar:*

Python - MNE-Python, iPython/jupiter notebooks, numpy, seaborn, pandas 

*Basic:*

Web Development - HTML/CSS/JS

Deplyments of wiki and forum infrastructures

<br/>
<br/>
<br/>
<br/>
<br/>

## Publications

<!-- A list is also available [online](http://scholar.google.co.uk/citations?user=LTOTl0YAAAAJ) -->

### Journal articles, in preparation

Marinato, G., & Baldauf, D. Neural Correlates of Auditory Object-Based Attention studied with Magnetoencephalography and Naturalistic Soundscapes


### Journals

`2019`
Marinato, G., & Baldauf, D. (2019). Object-based attention in complex, naturalistic auditory streams. Scientific Reports, 9(1), 1–13. doi: [10.1038/s41598-019-39166-6](https://doi.org/10.1038/s41598-019-39166-6).


### Conference Posters

**Ten years of Mind/Brain Sciences**, *CIMeC, University of Trento*, Rovereto, 19-21 October 2017

**CAOS – Concepts, Actions, and Objects**, *CIMeC,University of Trento*, 2-4 May 2019

**RAW – Rovereto Attention Workshop**, *CIMeC, University of Trento*, October 24 - 26, 2019



## University services

`2018-present`
**Board member**, *Open Science Seminars*, *CIMeC, University of Trento*, Italy.

`2016-present`
**PhD Representative**, *CIMeC, University of Trento*, Italy.



## Personal Projects

`2017 - present`
Co-organizer and founder, <a href="https://laforesta.net/">LaForesta</a><br/>
Community, art and research space in a former train station. Teaching basic informatics skills to minorities.

`2018`
Co-organizer, Wikipedia4Refugees
Project aimed at reducing digital skills gap


## Languages

Native: Italian

Other Languages: English (C1)
